from __future__ import print_function

from .objects import ContextualView, get_contextual_dict, get_extended_contextual_dict

from . import _version
__version__ = _version.get_versions()['version']
