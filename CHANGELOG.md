# Changelog

## [0.2.0] - 2024-01-31

### Added

issue #6 - New session view type allows to display and edit (when possible) the contextual dict of any flow object. An *Active view* option allows to always display the contextual dict of the object in the session's active view (disabled by default).

### Changed

Versioneer has been updated.

## [0.1.2] - 2020-11-04

Initial release